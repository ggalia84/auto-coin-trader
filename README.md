# README

This project provide an automatic trader for crypto coins. 

**Getting started**

**ENV**

First we'll need to create and edit the .env file:

$ cp .env.sample .env

$ sudo nano .env

**GEMS**

$ rvm use ruby-2.6.2@coin-auto-trader --create

$ rvm gemset list

$ bundle install

**DATABASE**

Generate the database with rails commands:

$ rails db:create

$ rails db:migrate

$ rails db:seed

**Starting the server**

$ rails s -p 3000

Or the port you want it to run.
