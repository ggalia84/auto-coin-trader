class ApplicationController < ActionController::Base
  protect_from_forgery prepend: true
  before_action :set_locale

  include Response
  include ExceptionHandler

  private

  # Select language
  def set_locale
    I18n.locale = params[:locale] || extract_locale_from_accept_language_header || I18n.default_locale
  rescue StandardError
    I18n.default_locale
  end

  # Extract language from default language of HTTP header
  def extract_locale_from_accept_language_header
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  rescue StandardError
    I18n.default_locale
  end

  # Add language param to all URLs automatically
  def default_url_options
    { locale: params[:locale] || :en }
  end

  def default_url_options(_options = {})
    Rails.env.production? ? { protocol: 'https', locale: I18n.locale } : { protocol: 'http', locale: I18n.locale }
  end
end
